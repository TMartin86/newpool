import { createRouter, createWebHistory } from "vue-router";
import store from "../store/index.js";
/* import App from "../../src/App.vue";
 */import Home from "../views/Home.vue";

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  {
    path: "/about",
    name: "About",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/About.vue"),
  },
  {
    path: "/login",
    name: "Login",
    component: () =>
      import(/* webpackChunkName: "about" */ "../components/auth/Login.vue"),
  },
 
  /* send all other requests to not found */
 /*  {
    path:"/:catchAll(.*)",
    name:"notfound",
    component: () => import("../views/NotFound.vue"),
  } */
];




const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

/* 
    router guard
    - runs between each nav change, prior to navigation
    - checks for auth and valid tokens 
    - attempts to get a new token if token is invalid, send to login if no token is generated
    - none authenticated routes will be sent to login
*/
router.beforeEach(async (to, from, next) => {
  console.log(to, from)
  let auth = store.getters['isAuthenticated'];
  
  // not authenticated and trying to go somewhere other than login
  if (to.name !== 'Login' && !auth){
     next({ name: 'Login' })
  }
  else {
    if (to.name !== 'Login'){
       // validate the token is still good
      let result = await store.dispatch('validateToken')
      
      // proceed
      if(result){
        next();
      }
      else{
      
        //attempt to get a new token
        let getNewToken = await store.dispatch('getNewToken');
        
        //if we get a new token we'll proceed
        if(getNewToken){
          next();
        } 

        // couldn't get a new token for some reason (slow connection maybe), head to login
        else{
          console.log("token error");
          await store.dispatch('logout');
          next({name: 'Login'})
        }
      }
    }
    
    else{next()}
    
  }
})

export default router;
