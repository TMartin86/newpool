import { constants } from "./constants.js";
export default {
    login: async (data = { email: "", password: "" }) => {
        if(data.email && data.username){
            const url = "https://api.promoonly.com/user/authenticate/signin";
            try{
                const response = await fetch(url, {
                    method: "POST",
                    headers: {
                        "Content-Type": "json"
                    },
                    body: JSON.stringify(data),
                })
                console.log("response ", response)
                const json = await response.json();
                console.log(json)
                return json;
            }catch(error){
                console.log(error);
                return(error);
            }
            
        }
        else{
            return(constants.LOGIN.GENERAL_ERROR);
        }
    }
};
