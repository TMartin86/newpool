export default {
    b64EncodeUnicode(str) {
        return btoa(
            encodeURIComponent(str).replace(/%([0-9A-F]{2})/g, function(
                match,
                p1
            ) {
                return String.fromCharCode("0x" + p1);
            })
        );
    },
    handleEncodedCharsForTracks(tracks = []) {
        //create one text area which we'll write too but never render to dom
        let txt = document.createElement("textarea");

        //loop through tracks
        for (const track of tracks) {
            //handle artist
            txt.innerHTML = track.artist;
            track.artist = txt.value;

            //clear html
            txt.innerHTML = "";

            //handle title
            txt.innerHTML = track.title;
            track.title = txt.value;

            //clear html
            txt.innerHTML = "";

            //handle mix
            txt.innerHTML = track.mix;
            track.mix = txt.value;

            //clear html
            txt.innerHTML = "";

            //handle genre
            txt.innerHTML = track.genre;
            track.genre = txt.value;
        }

        //set to null for garbage collection
        txt = null;
        return tracks;
    },

    handleEncodedChars: function(data) {
        const txt = document.createElement("textarea");
        txt.innerHTML = data;
        return txt.value;
    },

    todayString: function() {
        const today = new Date();
        const year = today.getFullYear();

        //add a leading zero for months and days less than 10
        const month =
            today.getMonth() + 1 < 10
                ? "0" + (today.getMonth() + 1)
                : today.getMonth() + 1;
        const day =
            today.getDate() < 10 ? "0" + today.getDate() : today.getDate();

        //return year + "-" + month + "-" + day;

        return `${year}-${month}-${day}`
    },

    animateSideBarClose: function() {
        //this.showSideBar = false;

        document.body.style.overflow = "hidden";

        //remove width from sidebar and remove margin from main
      /*   $("#sideBar").css("width", 0);
        $("#main").css("margin-left", 0);
 */
        //make the release tool bar fully expaned
        /*    $('#release-toolbar').addClass('toolbar-full');
        $('#release-toolbar').removeClass('toolbar-lg'); */

        //make the search tool bar fully expanded
     /*    $("#search-toolbar").addClass("toolbar-full");
        $("#search-toolbar").removeClass("toolbar-lg"); */

        //when advanced search is showing, adjust the input widths

        //adjust the options tool bar
        /*   $('#options-toolbar').addClass('toolbar-full');
        $('#options-toolbar').removeClass('toolbar-lg'); */

        //adjust the options tool bar
    /*     $("#specialty-toolbar").addClass("toolbar-full");
        $("#specialty-toolbar").removeClass("toolbar-lg"); */

        document.body.style.overflow = "visible";

        //adjust the options tool bar
        /*   $('#downloadQueue-toolbar').addClass('toolbar-full');
        $('#downloadQueue-toolbar').removeClass('toolbar-lg'); */
    },

    animateSideBarOpen: function() {
        //this.showSideBar = true;
        //add width to side bar and margin to main
        document.body.style.overflow = "hidden";

      /*   $("#sideBar").css("width", 300);
        $("#main").css("margin-left", 300);
 */
        //adjust the release tool bar
        /*  $('#release-toolbar').addClass('toolbar-lg');
        $('#release-toolbar').removeClass('toolbar-full');
*/
        //adjust the search tool bar
        /*   $('#search-toolbar').addClass('toolbar-lg');
        $('#search-toolbar').removeClass('toolbar-full');
 */
        //adjust the options tool bar
       /*  $("#options-toolbar").addClass("toolbar-lg");
        $("#options-toolbar").removeClass("toolbar-full"); */

        //adjust the specialty tool bar
        /* $("#specialty-toolbar").addClass("toolbar-lg");
        $("#specialty-toolbar").removeClass("toolbar-full"); */

        document.body.style.overflow = "visible";

        //adjust the specialty tool bar
        /*    $('#downloadQueue-toolbar').addClass('toolbar-lg');
        $('#downloadQueue-toolbar').removeClass('toolbar-full'); */
    },

    //initialize tooltips
    initToolTips: function() {
      /*   $('[data-toggle^="tooltip"]').tooltip({
            trigger: "hover",
        }); */
    },

    contentModified(id) {
        console.log(id);
    },

    // time used to keep token current if user goes long periods without clicking
    Timer(func, time = 4.5) {
        let timeObj = setInterval(func, time * 60 * 1000);

        // stop the timer
        this.stop = () => {
            if (timeObj) {
                clearInterval(timeObj);
                timeObj = null;
            }
            return this;
        };

        // start the timer
        this.start = () => {
            if (!timeObj) {
                this.stop();
                timeObj = setInterval(func, time * 60 * 1000);
            }
            return this;
        };

        // reset the timer
        this.reset = (newTime = time) => {
            time = newTime;
            return this.stop().start();
        };
    },

    // generic debounce function to help debounce events
    debounce(callback, wait){
        let timeoutId = null;
        return (...args) => {
          window.clearTimeout(timeoutId);
          timeoutId = window.setTimeout(() => {
            callback.apply(null, args);
          }, wait);
        };
    }
};
