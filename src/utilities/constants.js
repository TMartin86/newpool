export const constants = {
    NUM_RELEASES_PER_PAGE: 7,
    NUM_TRACKS_PER_PAGE: 7,
    NUM_GENRES_PER_PAGE: 7,
    NUM_FORMATS_PER_PAGE: 7,
    NUM_TRACKS_PER_TABLE_PAGE: 10,
    HISTORY_MODES: {
        ALL: 0,
        VIDEO: 1,
        AUDIO: 2,
    },
    LOGIN: {
        AUTH_SUCCESS: "Authenticated",
        AUTH_ERROR: "Error Authenticating",
        GENERAL_ERROR: "Error Authenticaing",
    },
};
