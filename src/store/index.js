
import { createStore } from "vuex";
import  helpers  from "../utilities/helpers"
import { constants } from "../utilities/constants";
/* import api  from "../utilities/api.js"; */
import router from "../router/index.js"; 


export default createStore({
    state: {
        loggedIn: false,
        loggedOut: false,
        loggingIn: false,
        loginError: false,
        loginErrorMsg: "",
        authStatus: false,
        token: null,
        userid: null,
        userEmail: null,
        secret: null,
        apiKey: null,
        tokenExpires: null,
        serverTime: null,
        previewQueue: {
            tracks: [],
            currentTrack:"",
        },
        previewQueueList: [],
        previewQueueTracks: {},
        previewPlaying: false,
        showLoader: false,
        pageNumber: 1,
        downloadQueueCount: 0,
        downloadQueue: [],
        showToolbar: "releases",
        formatId: 0,
        sideBarOpen: true,
        decadeMode: false,
        activeDecade: 2020,
        /*  filters:{
            genreFilters:{},
            mediaFilters:{},
            versionFilters:{},
            bpmFilters:{},
            downloadFilters:{},
        }, */
        customFilters: { Compilation: { hide: 0 } },
        filters: {
            Top_40: {
                filter: "Top 40",
                hide: 0,
                id: 1,
            },
            Urban: {
                filter: "Urban",
                hide: 0,
                id: 2,
            },
            Electronic: {
                filter: "Electronic",
                hide: 0,
                id: 3,
            },
            Dance: {
                filter: "Dance",
                hide: 0,
                id: 4,
            },
            Latin: {
                filter: "Latin",
                hide: 0,
                id: 5,
            },
            Country: {
                filter: "Country",
                hide: 0,
                id: 6,
            },
            Rock: {
                filter: "Rock",
                hide: 0,
                id: 7,
            },
            Christian: {
                filter: "Christian",
                hide: 0,
                id: 8,
            },
            Audio: {
                filter: "Audio",
                hide: 0,
                id: 9,
            },
            Video: {
                filter: "Video",
                hide: 1,
                id: 10,
            },
            HD: {
                filter: "HD",
                hide: 0,
                id: 11,
            },
            "Content Warnings": {
                filter: "Content Warnings",
                hide: 0,
                id: 12,
            },
            "Only Singles": {
                filter: "Only Singles",
                hide: 0,
                id: 13,
            },
            Alternative: {
                filter: "Alternative",
                hide: 0,
                id: 14,
            },
            "Specialty Releases": {
                filter: "Specialty Releases",
                hide: 0,
                id: 15,
            },
            "Show Explicit Content": {
                filter: "Show Explicit Content",
                hide: 0,
                id: 16,
            },
            "Show Noteworthy Only": {
                filter: "Show Noteworthy Only",
                hide: 0,
                id: 17,
            },
        },
        userInfo: {},
        userSettings: {},
        chartFilters: {},
        formatFilters: {},
        downloadFormats: {},
        downloadFilters: { hide: 0 },
        fileNaming: {},
        mediaFilters: {},
        trackControlQueue: {},
        releaseControlQueue: {},
        currentTrack: {},
        trackDownloading: false,
        downloadingTracks: [],
        removeQueue: [],
        userBrowser: "",
        previewTrack: {},
        removeQueuedIndex: null,
        formatIdsByFormat: {},
        formatIdsById: {},
        formatGenreMap: {},
        showExtraInfo: false,
        releases: {
            link: '',
            pageNumber:1,
            pages: {},
            releases: [],
            dates:{}
        },
        tracks:{

        },
        globalLoading: false,
        mobileOverlayTrack: {}

    },
    getters: {
        isAuthenticated: (state) => {
            const token = localStorage.getItem("token");
            if (
                token === state.token &&
                (token != null || token != undefined || token != "undefined")
            ) {
                return !!state.token;
            } else {
                return false;
            }
        },
        authStatus: (state) => state.authStatus,
        userid: (state) => state.userid,
        userEmail: (state) => state.userEmail,
        previewQueueList: (state) => state.previewQueueList,
        previewQueueTracks: (state) => state.previewQueueTracks,
        previewPlaying: (state) => state.previewPlaying,
        showLoader: (state) => state.showLoader,
        token: (state) => state.token,
        pageNumber: (state) => state.pageNumber,
        downloadQueueCount: (state) => state.downloadQueueCount,
        downloadQueue: (state) => state.downloadQueue,
        trackControlQueue: (state) => state.trackControlQueue,
        releaseControlQueue: (state) => state.releaseControlQueue,
        tokenExpires: (state) => state.tokenExpires,
        serverTime: (state) => state.serverTime,
        showToolbar: (state) => state.showToolbar,
        formatId: (state) => state.formatId,
        sideBarOpen: (state) => state.sideBarOpen,
        loginError: (state) => state.loginError,
        decadeMode: (state) => state.decadeMode,
        activeDecade: (state) => state.activeDecade,
        userInfo: (state) => state.userInfo,
        userSettings: (state) => state.userSettings,
        customFilters: (state) => state.customFilters,
        downloadFilters: (state) => state.downloadFilters,
        filters: (state) => state.filters,
        chartFilters: (state) => state.chartFilters,
        formatFilters: (state) => state.formatFilters,
        mediaFilters: (state) => state.mediaFilters,
        downloadFormats: (state) => state.downloadFormats,
        fileNaming: (state) => state.fileNaming,
        currentTrack: (state) => state.currentTrack,
        trackDownloading: (state) => state.trackDownloading,
        downloadingTracks: (state) => state.downloadingTracks,
        removeQueue: (state) => state.removeQueue,
        userBrowser: (state) => state.userBrowser,
        previewTrack: (state) => state.previewTrack,
        lastRemovedQueuedIndex: (state) => state.lastRemovedQueuedIndex,
        formatIdsByFormat: (state) => state.formatIdsByFormat,
        formatIdsById: (state) => state.formatIdsById,
        formatGenreMap: (state) => state.formatGenreMap,
        loggingIn: (state) => state.loggingIn,
        loginErrorMsg: (state) => state.loginErrorMsg,
        showExtraInfo: (state) => state.showExtraInfo,
        releases: (state) => state.releases,
        tracks: (state) => state.tracks,
        globalLoading: (state) => state.globalLoading,
        mobileOverlayTrack: (state) => state.mobileOverlayTrack
    },
    mutations: {
        loggedIn: (state) => (state.loggedIn = true),
        loggedOut: (state) => (state.loggedIn = false),
        setLoggingIn: (state, loggingIn) => (state.loggingIn = loggingIn),
        setUserId: (state, userid) => (state.userid = userid),
        setApiKey: (state, apiKey) => (state.apiKey = apiKey),
        setToken: (state, token) => (state.token = token),
        setExpiration: (state, exp) => (state.expiration = exp),
        setUserEmail: (state, userEmail) => (state.userEmail = userEmail),
        setUserInfo: (state, userInfo) => (state.userInfo = userInfo),
        setSecret: (state, secret) => (state.secret = secret),
        setAuthStatus: (state, authStatus) => (state.authStatus = authStatus),
        loginStart: (state) => {
            state.loggingIn = true;
            state.loginError = false;
            state.loginErrorMsg = "";
        },
        loginStop: (state, error = { error: false, msg: "" }) => {
            state.loggingIn = false;
            state.loginError = error.error;
            state.loginErrorMsg = error.msg;
        },
        clearLoginError: (state) =>  state.loginError = null,
        setReleases: (state, releases) => {
            let copy = {...state.releases};
            let {pageNumber, pages, dates} = copy;
            
            // if the page doesn't exist already
            if(!(pageNumber in pages) || pageNumber == 1){
                let newPage = {};
                
                // add the releases to the page
                releases.forEach((release) => {
                    let releaseCopy = [...release.releases];
                    newPage[release.date] = releaseCopy;
                    copy.releases = [...copy.releases, [...releaseCopy]];
                    
                    // check that date isn't already in the dates list
                    //if(!(release.date in dates)){
                        dates[release.date] = [...releaseCopy];
                   /*  }
                    else{
                        dates[release.date] = 
                    } */
                })

                pages[pageNumber] = newPage;
            }

            // page exists already and this is a refresh most likely
          /*   else{
                pages[pageNumber]
            } */
           
            copy.pageNumber += 1;
            state.releases = copy;
        },
        setTracks: (state, {releaseid, tracks}) => {
            let copy = {...state.tracks};
            if(!(releaseid in copy)){
                copy[releaseid] = tracks;
            }
            state.tracks = copy
        },
        setPreviewQueue: (state, track) => {
            let copy = {...state.previewQueue};
            if(!(track.titleid in copy)){
                let trackCopy = {...track}
                copy[track.titleid] = trackCopy;
                copy.tracks.push(trackCopy);
                copy.currentTrack = trackCopy;
            }
            state.previewQueue = copy;
            //https://stinger.promoonly.com/pi/pool_preview.php?titleid=661311&format=mp3&userid=32371
        },
        reloadReleases: (state) => {
            let copy = {...state.releases};
            copy.releases = [];
            
            // remove first page dates as they'll be populated again on refresh
            Object.entries(copy.dates).forEach((date) => {
                if((date in copy.pages['1']))
                    date = [];
            });


            copy.pages['1'] = {};
            copy.pageNumber = 1 ;
            state.releases = copy;

        },
        globalLoading: (state, trueOrFalse ) => {
            if(typeof trueOrFalse === "boolean")
                state.globalLoading = trueOrFalse;
            return;
        },
        setMobileOverlayTrack: (state, track) => {
            state.mobileOverlayTrack = track;
        }
    },
    actions: {
        async login({ commit }, data) {
            commit("loginStart");

            // configure form data for request
            const {email, password} = data;
            const formData = new FormData();
            formData.append("email", email);
            formData.append("password", password);

            // url and options for request
            const url = "https://api.promoonly.com/user/authenticate/signin"; 
            const options = {
                method: "POST",
                body: formData,
                redirect: 'follow'
            }

            // return a promise to fetch api data
            fetch(url, options)
            .then((response) => response.json())
            .then((data) => { 

                // success
                if (data.result === constants.LOGIN.AUTH_SUCCESS) {
                    router.push({ path: "/" });

                    // set local storage
                    localStorage.setItem("api_key", data.api_key);
                    localStorage.setItem("expiration", data.expiration);
                    localStorage.setItem("server_time", data.server_time);
                    localStorage.setItem("token", data.token);
                    localStorage.setItem("userid", data.userid);
    
                    commit("setUserId", data.userid);
                    commit("setUserEmail", email);
                    commit("setAuthStatus", true);
                    commit("setApiKey", data.api_key);
                    commit("setSecret", data.api_secret);
                    commit("setExpiration", data.expiration);
                    //commit('set', data.server_time);
                    //commit('setActiveDecade', 2020);
                    commit("setToken", data.token);
    
                    commit("loggedIn");
                    commit("loginStop");
                    
                    // navigate to home
                    router.push({ path: "/" });
                    console.log("logged in")
                } else {
                    commit("loggedOut");
                    commit("loginStop", { error: true, msg: data.result });
                } 
             })
            .catch((error) => {
                console.log(error)
                commit("loggedOut");
                commit("loginStop", { error: true, msg: error.message });
            })
        },
        async logout({commit},){
            commit('setAuthStatus', null);
            localStorage.removeItem("api_key");
            localStorage.removeItem("expiration");
            localStorage.removeItem("server_time");
            localStorage.removeItem("token");
            localStorage.removeItem("userid");

            commit("setUserId", null);
            commit("setUserEmail", null);
            commit("setAuthStatus", false);
            commit("setApiKey", null);
            commit("setSecret", null);
            commit("setExpiration", null);
            //commit('set', data.server_time);
            //commit('setActiveDecade', 2020);
            commit("setToken", null);
            commit("loggedOut");
            return;

        },
        async validateToken({ state}){
            const {userid, expiration, token} = state;

            // if there is a token to check
            if(token){
                // check if the current time is greater than the token expiration
                if(Math.round((new Date()).getTime() / 1000) >= expiration){

                    // configure form data for request
                    const formdata = new FormData();
                    formdata.append("userid", userid);
                    formdata.append("token", token);
    
                    // url and options for request
                    const url = "https://api.promoonly.com/user/token/validate"; 
                    const options = {
                        method: "POST",
                        body: formdata,
                        redirect: 'follow'
                    }
                    
                    // fetch data from api
                    try {
                        let response = await fetch(url, options)
                        if(response){
                            response = await response.json();
                            return response.token === "valid"
                        }
                        else{
                            return false;
                        }
                    } catch (error) {
                        return false;
                    }
                }
                // token is still good
                else{
                    return true;
                }
            }
            else{
                return false;
            }
        },
        async getNewToken({commit, state}){
            
            const {userid, apiKey, secret} = state;

            // set auth for header and form data
            const auth = helpers.b64EncodeUnicode(`${apiKey}:${secret}`);
            const authStr = `Basic ${auth}`;
            const formData = new FormData();
            formData.append("userid", userid);

            // url and options for request
            const url = "https://api.promoonly.com/user/authenticate"; 
            const options = {
                method: "POST",
                headers: {"Authorization" : authStr},
                body: formData,
                redirect: 'follow'
            }

            // fetch data from api
            try {
                let response = await fetch(url, options)
                if(response){
                    response = await response.json();
                    localStorage.setItem("token", response.token);
                    localStorage.setItem("expiration", response.expires);
                    commit('setToken', response.token);
                    commit('setExpiration', response.expires);
                    return true;
                }
                else{
                    return false;
                }
            } catch (error) {
                return false;
            }
        },
        async validateAndGetNew({dispatch}){
            let result = await dispatch('validateToken')
            console.log("result of new token in validate and get new: ", result);
            // proceed
            if(result){
                return true;
            }
            else{
            
              //attempt to get a new token
              let getNewToken = await dispatch('getNewToken');
              console.log(getNewToken);
              //if we get a new token we'll proceed
              if(getNewToken){
                  console.log("got new token")
                return true;
              } 
      
              // couldn't get a new token for some reason (slow connection maybe), head to login
              else{
                console.log("token error");

                return false;
               // return await dispatch('logout');
                
              }
            }
        },
        async getReleases({state, commit, dispatch}){
         
            let proceed = await dispatch('validateAndGetNew')
            if(proceed){
                const {userid, token} = state;
                const url = `https://api.promoonly.com/releases/date/${state.releases.pageNumber}/${constants.NUM_RELEASES_PER_PAGE}`;
                const authStr = `Bearer ${helpers.b64EncodeUnicode(userid + ":" + token)}`;
                const options = { headers: {"Authorization" : authStr}}
                
                try {
                    let response = await fetch(url, options)
                    if(response){
                        response = await response.json();
                        commit("setReleases", response);

                        return true;
                    }  
                } catch (error) {
                    console.log(error)
                }
            }
        },
        async reloadReleases({state, commit,  dispatch}){
            if(!state.globalLoading){
                commit("reloadReleases");
                commit("globalLoading", true);
                await dispatch("getReleases");
                commit("globalLoading", false);
                return true;
            }
            else{
                return false;
            }
        },
        
        async getTracks({state, commit, dispatch}, releaseid){
            let proceed = await dispatch('validateAndGetNew')
           
            if(proceed){
                // only get what we need from api
                if(!(releaseid in state.tracks)){
                    const {userid, token} = state;
                    const url = `https://api.promoonly.com/release/${releaseid}`;
                    const authStr = `Bearer ${helpers.b64EncodeUnicode(userid + ":" + token)}`;
                    const options = { headers: {"Authorization" : authStr}}
                    
                    try {
                    let response = await fetch(url, options)
                    if(response){
                        response = await response.json();
                        let tracks = response.tracks;
                        commit("setTracks", {releaseid, tracks })
                        return response.tracks;
                    }  
                    } catch (error) {
                        console.log(error)
                    }
                }
                else{
                    return state.tracks[releaseid]
                }
            }

           
           

        },
        addToPreviewQueue({state, commit}, track){
            if(!(track in state.previewQueue)){
                commit('setPreviewQueue');
            }else{
                return false;
            }
        },
        clearLoginError({commit}){
            commit('clearLoginError');
        },
        openNav(){
            requestAnimationFrame(() => {
                document.getElementById("mySidenav").classList.add('w-50-mobile');
                //document.getElementById("overlay").style.width = "100%";
                document.getElementById("toggle").checked = true;
            })
            return;
   
        },
        closeNav(){
            requestAnimationFrame(() => {

                document.getElementById("mySidenav").style.width = "0";
                document.getElementById("mySidenav").classList.remove('w-50-mobile');
                
                //document.getElementById("overlay").style.width = "0";
                document.getElementById("toggle").checked = false;
            })
           
            return;
        },
        openMobileActionOverlay({commit}, track = {}){
            commit("setMobileOverlayTrack", track);
            requestAnimationFrame(() => {
                document.getElementById("myNav").style.display = "block";
            })
        },
        closeMobileActionOverlay(){
            this.commit("setMobileOverlayTrack", {});
            requestAnimationFrame(() => {
                document.getElementById("myNav").style.display = "none";
            })
        }
    },
  modules: {

  },
});

